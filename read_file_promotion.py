'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
'''
# Boucle de lecture ligne à ligne
with open('promotion_B3_B.csv', 'r') as file :
    # On saute la premier ligne
    next(file)
    for line in file:
        fields = line.split(";")
        print(f"{fields[1]} - {fields[0]}")

print("fin du programme...")
    
