'''
    Programme qui lit un fichier et "parse" une ligne en champs
'''
import datetime as dt
import functions.application_functions as ft

fpromo=open("promotion_B3_B.csv")
lnumber=-1
today=dt.datetime.today()
for line in fpromo:
    lnumber+=1
    # On ne traite pas la première ligne (entête)
    if lnumber == 0 :
        continue

    fields=line.strip().split(";")
    name=fields[0]
    fname=fields[1]
    gender=fields[2]
    email=fields[3]
    bdate=fields[4]
    # print(f"Nom:{name}, prénom:{fname}, email={email}, date de naissance:{bdate}")
    bdate = dt.datetime.strptime(bdate,'%d/%m/%Y')
    # delta = ft.gdelta(bdate, today)
    nbMois = ft.nbMonths(bdate, today)
    print(f"Nom:{name}, prénom:{fname}, gender:{gender}, bdate :{bdate}, nbMois :{nbMois}")
fpromo.close
print("Fin du programme...")